package com.example.monumento.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.monumento.LoginActivity;
import com.example.monumento.R;
import com.example.monumento.ResetPasswordActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class MoreFragment extends Fragment {
    private TextView signout,Terms,About,Reset;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private ProgressDialog pDialog;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more,container,false);
        signout=view.findViewById(R.id.button_signout);
        Terms=view.findViewById(R.id.button_terms);
        About=view.findViewById(R.id.button_about_us);
        Reset=view.findViewById(R.id.button_reset_password);
        signout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                FirebaseAuth.getInstance().signOut();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user == null) {

                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);                }

            }
        });
        Reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ResetPasswordActivity.class);
                startActivity(intent);
            }
        });



        Terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder=new AlertDialog.Builder(getContext());

               View view=getLayoutInflater().inflate(R.layout.popup,null);
                TextView textView=view.findViewById(R.id.popList);
                Button close=view.findViewById(R.id.dismissPopTop);
                Button close_text=view.findViewById(R.id.dismissPop);
                Reset=view.findViewById(R.id.button_reset_password);
//                Reset.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
                textView.setText("This is an example text for Monumento Terms and conditions");

                pDialog = new ProgressDialog(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                pDialog.setTitle("PDF");
                pDialog.setMessage("Loading...");

                close_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });


                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialogBuilder.setView(view);
                dialog = dialogBuilder.create();
                dialog.show();

            }
        });
        About.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder=new AlertDialog.Builder(getContext());

                View view=getLayoutInflater().inflate(R.layout.popup_aboutus,null);
                TextView textView=view.findViewById(R.id.popList);
                Button close=view.findViewById(R.id.dismissPopTop);
                Button close_text=view.findViewById(R.id.dismissPop);
                textView.setText("This is an example text for Monumento About Info");

                pDialog = new ProgressDialog(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                pDialog.setTitle("PDF");
                pDialog.setMessage("Loading...");

                close_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });


                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialogBuilder.setView(view);
                dialog = dialogBuilder.create();
                dialog.show();


            }
        });
        return view;
    }
}
