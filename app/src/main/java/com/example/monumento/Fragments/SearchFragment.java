package com.example.monumento.Fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.monumento.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.ContextCompat.getSystemService;
import static com.example.monumento.SignUpActivity.PICK_IMAGE;

public class SearchFragment extends Fragment {
    Button flash,gallery,know_more,ar_move,save;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private ProgressDialog pDialog;
    private static final int CAMERA_REQUEST = 50;
    private boolean flashLightStatus = false;
    final int REQUEST_PERMISSION_CODE=1000;
    private ImageView imageview;
    private BottomSheetBehavior bottomSheetBehavior;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search,container,false);
flash=view.findViewById(R.id.button_toggle_flash);
gallery=view.findViewById(R.id.button_gallery);
        FrameLayout bottomSheetButton = view.findViewById(R.id.bottom_sheet_button);
        imageview=view.findViewById(R.id.face_detection_camera_image_view);
        know_more=view.findViewById(R.id.button_know_more);
        ar_move=view.findViewById(R.id.button_AR);
        save=view.findViewById(R.id.button_Save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder=new AlertDialog.Builder(getContext());

                View view=getLayoutInflater().inflate(R.layout.popup_save,null);
                TextView textView=view.findViewById(R.id.popList);
                TextView close_text=view.findViewById(R.id.dismissPop);


                pDialog = new ProgressDialog(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                pDialog.setTitle("PDF");
                pDialog.setMessage("Loading...");

                close_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });



                dialogBuilder.setView(view);
                dialog = dialogBuilder.create();
                dialog.show();

            }
        });
        ar_move.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, new ARFragment());
                transaction.commit();
            }
        });
        know_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder=new AlertDialog.Builder(getContext());

                View view=getLayoutInflater().inflate(R.layout.popup_know_more,null);
                TextView textView=view.findViewById(R.id.popList);
                Button close=view.findViewById(R.id.dismissPopTop);
                Button close_text=view.findViewById(R.id.dismissPop);

                textView.setText("he Burj Khalifa (Arabic: برج خليفة\u200E, Arabic for \"Khalifa Tower\"; pronounced English: /ˈbɜːrdʒ kəˈliːfə/), known as the Burj Dubai prior to its inauguration in 2010, is a skyscraper in Dubai, United Arab Emirates. With a total height of 829.8 m (2,722 ft) and a roof height (excluding antenna, but including a 244 m spire[2]) of 828 m (2,717 ft), the Burj Khalifa has been the tallest structure and building in the world since its topping out in 2009.[3][4]\n" +
                        "\n" +
                        "Construction of the Burj Khalifa began in 2004, with the exterior completed five years later in 2009. The primary structure is reinforced concrete. The building was opened in 2010 as part of a new development called Downtown Dubai. It is designed to be the centrepiece of large-scale, mixed-use development. The decision to construct the building is based on the government's decision to diversify from an oil-based economy, and for Dubai to gain international recognition. The building was originally named Burj Dubai but was renamed in honour of the ruler of Abu Dhabi and president of the United Arab Emirates, Khalifa bin Zayed Al Nahyan;[5] Abu Dhabi and the UAE government lent Dubai money to pay its debts. The building broke numerous height records, including its designation as the tallest building in the world.\n" +
                        "\n" +
                        "Burj Khalifa was designed by Adrian Smith, of Skidmore, Owings & Merrill, whose firm designed the Willis Tower and One World Trade Center. Hyder Consulting was chosen to be the supervising engineer with NORR Group Consultants International Limited chosen to supervise the architecture of the project. The design is derived from the Islamic architecture of the region, such as in the Great Mosque of Samarra. The Y-shaped tripartite floor geometry is designed to optimize residential and hotel space. A buttressed central core and wings are used to support the height of the building. Although this design was derived from Tower Palace III, the Burj Khalifa's central core houses all vertical transportation with the exception of egress stairs within each of the wings.[6] The structure also features a cladding system which is designed to withstand Dubai's hot summer temperatures. It contains a total of 57 elevators and 8 escalators.");

                pDialog = new ProgressDialog(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                pDialog.setTitle("PDF");
                pDialog.setMessage("Loading...");

                close_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });


                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialogBuilder.setView(view);
                dialog = dialogBuilder.create();
                dialog.show();

            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        final boolean hasCameraFlash = getContext().getPackageManager().
                hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if(!checkPermissionFromDevice())
            requestPermission();
        flash.setEnabled(true);

flash.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (hasCameraFlash) {
            if (flashLightStatus)
                flashLightOff();
            else
                flashLightOn();

        } else
            Toast.makeText(getContext(), "No flash available on your device",
                    Toast.LENGTH_SHORT).show();
    }
});
        return view;
    }
    @TargetApi(Build.VERSION_CODES.M)
    private void flashLightOn() {
        CameraManager cameraManager = (CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE);

        if(checkPermissionFromDevice()){try {
            String cameraId = cameraManager.getCameraIdList()[0];
            cameraManager.setTorchMode(cameraId, true);
            flashLightStatus = true;
            //imageFlashlight.setImageResource(R.drawable.btn_on);

        } catch (CameraAccessException e) {
        }
        }
        else
            requestPermission();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void flashLightOff() {

        CameraManager cameraManager = (CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE);

        try {
            String cameraId = cameraManager.getCameraIdList()[0];
            cameraManager.setTorchMode(cameraId, false);
            flashLightStatus = false;
           // imageFlashlight.setImageResource(R.drawable.btn_off);
        } catch (CameraAccessException e) {
        }
    }
    private void requestPermission()
    {
        ActivityCompat.requestPermissions((Activity) getContext(),new String[]{
                Manifest.permission.CAMERA,
        },REQUEST_PERMISSION_CODE );
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case CAMERA_REQUEST :
                if (grantResults.length > 0  &&  grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    flash.setEnabled(true);
                } else
                    Toast.makeText(getContext(), "Permission Denied for the Camera", Toast.LENGTH_SHORT).show();

                break;
        }
    }
    private boolean checkPermissionFromDevice()
    {

        int camera=
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);


        return camera == PackageManager.PERMISSION_GRANTED ;

    }
    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            imageview.setImageURI(imageUri);
        }
    }
}
