package com.example.monumento.Fragment_Profile;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.monumento.Adapter;
import com.example.monumento.Example_recycler;
import com.example.monumento.R;

import java.util.ArrayList;
import java.util.List;

public class places_visited_adapter extends RecyclerView.Adapter<places_visited_adapter.ExampleViewHolder> {
    private ArrayList<places_visited_recycler> mExampleList;

    public static class ExampleViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTextView1;
        public TextView mTextView2;



        public ExampleViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.main_image1);
            mTextView1 = itemView.findViewById(R.id.place1);
            mTextView2 = itemView.findViewById(R.id.date1);


        }
    }

    public places_visited_adapter(ArrayList<places_visited_recycler> exampleList) {
        mExampleList = exampleList;
    }

    @Override
    public places_visited_adapter.ExampleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.places_visited_recycler, parent, false);
        places_visited_adapter.ExampleViewHolder evh = new places_visited_adapter.ExampleViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {
        places_visited_recycler currentItem = mExampleList.get(position);

        holder.mImageView.setImageResource(currentItem.getImageResource());
        holder.mTextView1.setText(currentItem.getText1());
        holder.mTextView2.setText(currentItem.getText2());
    }





    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}
