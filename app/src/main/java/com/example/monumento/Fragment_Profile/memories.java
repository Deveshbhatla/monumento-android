package com.example.monumento.Fragment_Profile;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.monumento.Adapter;
import com.example.monumento.Data.User;
import com.example.monumento.Example_recycler;
import com.example.monumento.LoginActivity;
import com.example.monumento.MainActivity;
import com.example.monumento.R;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class memories extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_memories, container, false);


        ArrayList<Example_recycler> exampleList = new ArrayList<>();
        exampleList.add(new Example_recycler(R.drawable.burjkhalifa, "Burj Khalifa", "Dubai", "5/6/19", "wonderful trip"));
        exampleList.add(new Example_recycler(R.drawable.tajmahal, "Taj Mahal", "India", "4/5/20", "Just fabulous"));
        exampleList.add(new Example_recycler(R.drawable.burjkhalifa, "Burj Khalifa", "Dubai", "5/6/19", "wonderful trip"));
        exampleList.add(new Example_recycler(R.drawable.tajmahal, "Taj Mahal", "India", "4/5/20", "Just fabulous"));


        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new Adapter(exampleList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);



        return view;
    }

}
