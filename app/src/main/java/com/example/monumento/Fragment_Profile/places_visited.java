package com.example.monumento.Fragment_Profile;

import android.app.Activity;
import android.app.ListActivity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.monumento.Adapter;
import com.example.monumento.Example_recycler;
import com.example.monumento.R;

import java.util.ArrayList;
import java.util.Objects;

public class places_visited extends Fragment
{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_places, container, false);


            ArrayList<places_visited_recycler> exampleList1 = new ArrayList<>();
            exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));
        exampleList1.add(new places_visited_recycler(R.drawable.eiffel, "Eiffel Tower, Paris", "Visited On: 23/2/2020"));



        mRecyclerView = view.findViewById(R.id.recycler_view1);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getContext());
            mAdapter = new places_visited_adapter(exampleList1);

            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);



            return view;
        }
    }

