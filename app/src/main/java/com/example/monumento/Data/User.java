package com.example.monumento.Data;

public class User {
    private String displayName;
    private String email;
    private Number Phone;

    public Number getPhone() {
        return Phone;
    }

    public void setPhone(Number phone) {
        Phone = phone;
    }

    public String getBlood_Group() {
        return Blood_Group;
    }

    public void setBlood_Group(String blood_Group) {
        Blood_Group = blood_Group;
    }

    private String Blood_Group;
    private String dob;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
