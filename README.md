#Monumento-Android

Prototype for Monumento Android app for AOSSIE. 
The application has reached the stage where basic functionality like Authentication, Database, Reset Password, Sign Out and AR is working.
The AR is satic however. I am regularly adding more functionalities to the application and improving the User Interface by using cardviews and elevations.
The UI us still basic, although it gives an idea of the fundamental structure of the application.
This repository is a proof of implementing the ideas for which I created a merge request.

Here is the video of my implementation:
https://www.youtube.com/watch?v=WZbmLFZvCMQ&feature=youtu.be